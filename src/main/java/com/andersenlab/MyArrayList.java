package com.andersenlab;

import java.util.Iterator;

public class MyArrayList<T> implements Iterable<T> {

	private final int INIT_SIZE = 16;
	private final double INCREMENT_CAPACITY = 1.5;
	private final int CUT_SIZE = 4;
	private Object[] array = null;
	private int pointer = 0;

	public MyArrayList() {
		array = new Object[INIT_SIZE];
	}

	public void addElement(T item) {
		if (pointer == array.length - 1) {
			resize(array.length * 2);
		}
		array[pointer++] = item;
	}

	public T getElement(int index) {
		return (T) array[index];
	}

	public void deleteElement(T item) {
		for (int i = 0; i < pointer; i++) {
			try {
				if (array[i] == item) {
					deleteElement(i);
					break;
				}
			} catch (Exception e) {

				e.printStackTrace();
			}
		}
	}

	public void deleteElement(int index) {
		for (int i = index; i < pointer; i++) {
			array[i] = array[i + 1];
		}
		array[pointer] = null;
		pointer--;

		if (array.length > INIT_SIZE && pointer < array.length / CUT_SIZE) {
			resize(array.length / 2);
		}
	}

	public int size() {
		return pointer;
	}

	public void clear() {
		for (int i = 0; i < pointer; i++) {
			array[i] = null;
		}
		pointer = 0;
	}

	private void resize(int length) {
		Object[] newArray = new Object[length];
		System.arraycopy(array, 0, newArray, 0, pointer);
		array = newArray;
	}

	@Override
	public Iterator<T> iterator() {

		return new Iterator<T>() {

			private int iteratorPointer = 0;

			@Override
			public boolean hasNext() {
				if (array[iteratorPointer + 1] != null) {
					return true;
				}
				return false;
			}

			@Override
			public T next() {
				if (array[iteratorPointer] == null) {
					return null;
				}
				return (T) array[iteratorPointer];
			}
		};
	}

}
