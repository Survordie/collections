package com.andersenlab;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.andersenlab.MyArrayList;

public class MyArrayListTest {

	MyArrayList<String> myList = null;
	MyArrayList<String> shareList = new MyArrayList<>();

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void createListTest() {
		myList = new MyArrayList<>();

		Assert.assertTrue(myList.size() == 0);
	}

	@Test
	public void addElementTest() {
		shareList.addElement("Hello");

		Assert.assertTrue(shareList.size() == 1);

	}

	@Test
	public void checkAddedElementTest() {
		shareList.addElement("Hello");
		Assert.assertTrue("Hello".equals(shareList.getElement(0)));
	}

	@Test
	public void clearTest() {
		shareList.clear();
		Assert.assertTrue(shareList.size() == 0);
	}
}
